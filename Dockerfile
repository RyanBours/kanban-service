FROM node:14 AS base
WORKDIR /src

COPY package*.json yarn.lock ./
RUN yarn install --production
COPY . .
RUN yarn build

ENV PORT=8001

CMD [ "yarn", "prod" ]