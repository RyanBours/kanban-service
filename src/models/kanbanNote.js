import { model, Schema } from 'mongoose'

const kanbanNoteSchema = new Schema({
    content: {
        type: String,
        require: true,
    },
    lane: {
        type: Schema.Types.ObjectId,
        require: true,
    },
    order: {
        type: Number,
        require: true,
    }
}, {
    timestamps: {}
})

const KanbanNote = model('kanbannote', kanbanNoteSchema) // TODO: rename collection

export default KanbanNote
