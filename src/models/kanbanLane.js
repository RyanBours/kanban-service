import { model, Schema } from 'mongoose'

const kanbanLaneSchema = new Schema({
    title: {
        type: String,
        require: true,
    },
    board: {
        type: Schema.Types.ObjectId,
        require: true,
    },
    order: {
        type: Number,
        require: true,
    }
}, {
    timestamps: {}
})

const KanbanLane = model('kanbanlane', kanbanLaneSchema) // TODO: rename collection

export default KanbanLane
