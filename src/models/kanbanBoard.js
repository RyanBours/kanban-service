import { model, Schema } from 'mongoose'

const kanbanBoardSchema = new Schema({
    type: {
        type: String,
        require: true,
        default: 'Kanban'
    },
    title: {
        type: String,
        require: true,
    },
    workspace: {
        type: Schema.Types.ObjectId,
        require: true,
    },
}, {
    timestamps: {}
})

const KanbanBoard = model('kanbanBoard', kanbanBoardSchema) // TODO: rename collection

export default KanbanBoard
