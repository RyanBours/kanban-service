import { KanbanBoard, KanbanLane, KanbanNote } from '../../models/index'

const resolvers = {
    Mutation: {
        createKanbanBoard: async (_, { title, workspace }) => {
            // TODO: move to a service
            // TODO: check user authenticated
            // TODO: check user has access to workspace
            return await new KanbanBoard({ title, workspace }).save()
        },
        createKanbanLane: async (_, { title, board, order }) => {
            return await new KanbanLane({ title, board, order }).save()
        },
        createKanbanNote: async (_, { content, lane, order }) => {
            return await new KanbanNote({ content, lane, order }).save()
        },
        updateNoteOrder: async (_, { noteId, order }) => {
            const note = await KanbanNote.findById(noteId)
            note.order = order
            await note.save()
            return note
        },
        updateNoteLane: async (_, { noteId, laneId }) => {
            const note = await KanbanNote.findById(noteId)
            note.lane = laneId
            await note.save()
            return note
        },
        updateNoteContent: async (_, { noteId, content }) => {
            const note = await KanbanNote.findById(noteId)
            note.content = content
            await note.save()
            return note
        }
    },

    Query: {
        boards: async (parent, { workspace }, context) => {
            const kanbanBoards = await KanbanBoard.find({
                ...(!workspace || { workspace })
            })
            return [...kanbanBoards]
        },

        kanban: async (parent, args, context) => { return await KanbanBoard.findById(args.id) },
        kanbans: async () => { return await KanbanBoard.find() },
    },

    Workspace: {
        boards: async (parent, args, context) => {
            const kanbanBoards = await KanbanBoard.find({ workspace: parent.id })
            return [...kanbanBoards]
        }
    },

    Board: {
        __resolveType(board) {
            return board.type ?? null // TODO return error if no type
        }
    },

    Kanban: {
        __resolveReference(kanban) {
            return KanbanBoard.findById(kanban.id)
        },
        workspace: ({ workspace }) => {
            return { __typename: 'Workspace', id: workspace }
        },
        lanes: async ({ id }) => {
            return await KanbanLane.find({ board: id })
        },
        notes: async ({ id }) => {
            const lanes = await KanbanLane.find({ board: id })
            return await KanbanNote.find({ lane: { $in: lanes.map(lane => lane.id) } })
        }
    },
    Lane: {
        __resolveReference: async ({ id }) => {
            return await KanbanLane.findById(id)
        },
        board: ({ board }) => {
            return { __typename: 'Kanban', id: board }
        },
        notes: async ({ id }) => {
            return await KanbanNote.find({ lane: id }).sort('order')
        }
    },
    Note: {
        __resolveReference: async ({ id }) => {
            return await KanbanNote.findById(id)
        },
        lane: ({ lane }) => {
            return { __typename: 'Lane', id: lane }
        }
    },
}
export default resolvers
