import { gql } from 'apollo-server-core'

const typeDefs = gql`
    type Mutation {
        createKanbanBoard(title: String!, workspace: String!): Kanban
        createKanbanLane(title: String!, board: String!, order: Int!): Lane
        createKanbanNote(content: String!, lane: String!, order: Int!): Note
        updateNoteOrder(noteId: String!, order: Int!): Note
        updateNoteLane(noteId: String!, laneId: String!): Note
        updateNoteContent(noteId: String!, content: String!): Note
    }

    interface Board {
        id: ID!
        type: String
        title: String!
        workspace: Workspace
    }

    type Query {
        boards(workspace: ID): [Board!]!
        kanban(id: String): Kanban
        kanbans: [Kanban]
    }
    
    type Workspace @key(fields: "id") @extends {
        id: ID! @external
        boards: [Board]
    }
    
    type Kanban implements Board @key(fields: "id") {
        id: ID!
        type: String!
        title: String!
        workspace: Workspace!
        lanes: [Lane]
        notes: [Note]
    }
    type Lane @key(fields: "id"){
        id: ID!
        title: String!
        board: Kanban!
        order: Int!
        notes: [Note] 
    }
    type Note @key(fields: "id") {
        id: ID!
        lane: Lane!
        content: String!
        order: Int!
    }
`

export default typeDefs
